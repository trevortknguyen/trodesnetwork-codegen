module Trodes.Network.Types.HWGlobalStimulationCommand where

import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp, CppType)

data HWGlobalStimulationCommand = HWGlobalStimulationCommand
    { resetSequencerCmd :: Bool
    , killStimulationCmd :: Bool
    , clearDSPOffsetRemovalCmd :: Bool
    , enableStimulation :: Bool
    } deriving (Generic, Cpp, CppType)
