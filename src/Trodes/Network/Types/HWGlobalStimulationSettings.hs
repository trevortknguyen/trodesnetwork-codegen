module Trodes.Network.Types.HWGlobalStimulationSettings where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp, CppType)

data HWGlobalStimulationSettings = HWGlobalStimulationSettings
    { scaleValue :: Text
    } deriving (Generic, Cpp, CppType)

