module Trodes.Network.Messages.TrodesTimestampData where

import Data.Int
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesTimestampData = TrodesTimestampData
    { localTimestamp :: Word32
    , systemTimestamp :: Int64
    } deriving (Generic, Cpp)
