module Trodes.Network.Messages.TrodesEvent where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesEvent = TrodesEvent
    { tag :: Text
    } deriving (Generic, Cpp)
