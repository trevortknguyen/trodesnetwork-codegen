module Trodes.Network.Messages.TrodesDigitalData where

import Data.Int
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesDigitalData = TrodesDigitalData
    { localTimestamp :: Word32
    , digitalData :: [[Word8]]
    , systemTimestamp :: Int64
    } deriving (Generic, Cpp)
