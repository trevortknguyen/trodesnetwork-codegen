module Trodes.Network.Messages.TrodesRequest where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesRequest = TrodesRequest
    { tag :: Text
    } deriving (Generic, Cpp)
