module Trodes.Network.Cpp.Ast where

import Data.Text

data CppDefinition = CppDefinition
    { cppName :: Text
    , cppConstructors :: [CppConstructor]
    }

data CppConstructor = CppConstructor
    { cppConstructorName :: Text
    , cppConstructorVariables :: [CppRecordField]
    }

data CppRecordField = CppRecordField
    { cppRecordFieldType :: !TypeRef
    , cppRecordFieldName :: Text
    }

data CppPrim =
      CppBool
    | CppUint8
    | CppUint16
    | CppUint32
    | CppUint64
    | CppInt8
    | CppInt16
    | CppInt32
    | CppInt64
    | CppFloat
    | CppDouble
    | CppString
    | CppVector !TypeRef

newtype TypeName = TypeName
    { untypeName :: Text
    }

data TypeRef =
      RefPrim !CppPrim
    | RefCustom !TypeName