module Trodes.Network.Cpp.Print.Source where

import Data.Text (Text)
import qualified Data.Text as T
import Trodes.Network.Cpp.Ast

-- | This is not complete
prettyShowSource :: CppDefinition -> Text
prettyShowSource _ = T.unlines
    [ "namespace trodes {"
    , "namespace network {"
    , "// what"
    , "}  // namespace network"
    , "}  // namespace trodes"
    ]