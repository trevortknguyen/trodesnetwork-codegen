module Trodes.Network.Cpp.Print.StructBasedADT where

import Data.Text (Text)
import qualified Data.Text as T
import Trodes.Network.Cpp.Ast
import Trodes.Network.Cpp.Print.Types

-- iterate through each constructor and make it a struct
prettyShowType :: CppDefinition -> Text
prettyShowType def = T.unlines
    [ "#include <variant>"
    , "#include <string>"
    , "using std::variant;"
    , ""
    , T.unlines constructorImpls
    , variant
    ]
    where
        variant = "typedef std::variant<" <> constructors <> "> " <> className <> ";"
        constructorImpls = map prettyShowConstructor $ cppConstructors def
        constructors = T.intercalate ", " $ map cppConstructorName $ cppConstructors def   :: Text
        className = cppName def :: Text

prettyShowConstructor :: CppConstructor -> Text
prettyShowConstructor constructor = T.unlines
    [ "// constructor " <> cppConstructorName constructor
    , "struct " <> cppConstructorName constructor <> " {"
    , T.unlines fields
    , "};"
    ]
    where
        fields = map prettyShowField $ cppConstructorVariables constructor

prettyShowField :: CppRecordField -> Text
prettyShowField field = T.unlines
    [ "// field impl " <> cppRecordFieldName field
    , printField field
    ]
    where
        printField :: CppRecordField -> Text
        printField f = (printFieldType f) <> " " <> (printFieldName f) <> ";"

        printFieldType :: CppRecordField -> Text
        printFieldType f = docToText $ cppTypeRefDoc $ cppRecordFieldType f

        printFieldName :: CppRecordField -> Text
        printFieldName f = cppRecordFieldName f

        docToText = T.pack . show