{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
module Trodes.Network.Cpp.Generate where

import Data.Kind (Type)
import Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.ByteString as BS
import System.FilePath ((<.>), (</>))
import Trodes.Network.Cpp.Ast
import Trodes.Network.Cpp.Generic
import Trodes.Network.Cpp.Print.Header (prettyShowHeader)
import Trodes.Network.Cpp.Print.JuliaStruct as JS
import Trodes.Network.Cpp.Print.MessagePack as MP
import Trodes.Network.Cpp.Print.Source (prettyShowSource)
import Trodes.Network.Cpp.Print.StructBasedADT as SB

generateCpp :: forall (t :: Type) . Cpp t => IO ()
generateCpp = do
    writeCpp pathHeader (prettyShowHeader (toCppAst @t))
    writeCpp pathSource (prettyShowSource (toCppAst @t))

    where
        pathPrefix = "generated"
        pathHeader = pathPrefix </> (renderBasename @t "h")
        pathSource = pathPrefix </> (renderBasename @t "cpp")

generateStructBasedCpp :: forall (t :: Type) . Cpp t => IO ()
generateStructBasedCpp = do
    writeCpp path (SB.prettyShowType (toCppAst @t))
    where
        path = "generated" </> basename
        basename = renderBasename @t "h"

generateMessagePackStructCpp :: forall (t :: Type) . Cpp t => IO ()
generateMessagePackStructCpp = do
    writeCpp path (MP.prettyShowType (toCppAst @t))
    where
        path = "generated" </> (renderBasename @t "h")

generateJuliaStructCpp :: forall (t :: Type) . Cpp t => IO ()
generateJuliaStructCpp = do
    writeCpp path (JS.prettyShowType (toCppAst @t))
    where
        path = "generated" </> (renderBasename @t "jl")



-- internal

renderBasename :: forall (t :: Type) . Cpp t => FilePath -> FilePath
renderBasename ext = (renderFilename @t) <.> ext
    where
        renderFilename :: forall (t_ :: Type) . Cpp t_ => FilePath
        renderFilename = T.unpack $ cppName (toCppAst @t)

writeCpp :: FilePath -> Text -> IO ()
writeCpp path def = BS.writeFile path $ encodeUtf8 def
