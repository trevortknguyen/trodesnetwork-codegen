{-# LANGUAGE TypeApplications #-}
module Main where

import System.Directory (createDirectoryIfMissing)
import Trodes.Network.Cpp.Generate (generateMessagePackStructCpp)

import Trodes.Network.Messages.AcquisitionCommand as M
import Trodes.Network.Messages.AnnotationRequest as M
import Trodes.Network.Messages.Event as M
import Trodes.Network.Messages.FileStatus as M
import Trodes.Network.Types.HWClear as M
import Trodes.Network.Types.HWGlobalStimulationCommand as M
import Trodes.Network.Types.HWGlobalStimulationSettings as M
import Trodes.Network.Types.HWStartStop as M
import Trodes.Network.Types.HWStimulationCommand as M
import Trodes.Network.Types.HWTrigger as M
import Trodes.Network.Messages.Handshake as M
import Trodes.Network.Messages.HardwareRequest as M
import Trodes.Network.Types.ITime as M
import Trodes.Network.Types.ITimerate as M
import Trodes.Network.Messages.InfoRequest as M
import Trodes.Network.Messages.InfoResponse as M
import Trodes.Network.Messages.ServerRequestSimple as M
import Trodes.Network.Messages.SourceStatus as M
import Trodes.Network.Messages.SpikePacket as M
import Trodes.Network.Messages.TrodesAnalogData as M
import Trodes.Network.Messages.TrodesDigitalData as M
import Trodes.Network.Messages.TrodesEvent as M
import Trodes.Network.Messages.TrodesLFPData as M
import Trodes.Network.Messages.TrodesNeuralData as M
import Trodes.Network.Messages.TrodesNotification as M
import Trodes.Network.Messages.TrodesRequest as M
import Trodes.Network.Messages.TrodesSpikeWaveformData as M
import Trodes.Network.Messages.TrodesTimestampData as M
import Trodes.Network.Messages.VideoPacket as M

main :: IO ()
main = do
    createDirectoryIfMissing True "generated"
    putStrLn "Generating..."

    -- actual types used in Trodes
    generateMessagePackStructCpp @M.AcquisitionCommand

    generateMessagePackStructCpp @M.AcquisitionCommand
    generateMessagePackStructCpp @M.AnnotationRequest
    generateMessagePackStructCpp @M.Event
    generateMessagePackStructCpp @M.FileStatus
    generateMessagePackStructCpp @M.HWClear
    generateMessagePackStructCpp @M.HWGlobalStimulationCommand
    generateMessagePackStructCpp @M.HWGlobalStimulationSettings
    generateMessagePackStructCpp @M.HWStartStop
    generateMessagePackStructCpp @M.HWStimulationCommand
    generateMessagePackStructCpp @M.HWTrigger
    generateMessagePackStructCpp @M.Handshake
    generateMessagePackStructCpp @M.HardwareRequest
    generateMessagePackStructCpp @M.ITime
    generateMessagePackStructCpp @M.ITimerate
    generateMessagePackStructCpp @M.InfoRequest
    generateMessagePackStructCpp @M.InfoResponse
    generateMessagePackStructCpp @M.ServerRequestSimple
    generateMessagePackStructCpp @M.SourceStatus
    generateMessagePackStructCpp @M.SpikePacket
    generateMessagePackStructCpp @M.TrodesAnalogData
    generateMessagePackStructCpp @M.TrodesDigitalData
    generateMessagePackStructCpp @M.TrodesEvent
    generateMessagePackStructCpp @M.TrodesLFPData
    generateMessagePackStructCpp @M.TrodesNeuralData
    generateMessagePackStructCpp @M.TrodesNotification
    generateMessagePackStructCpp @M.TrodesRequest
    generateMessagePackStructCpp @M.TrodesSpikeWaveformData
    generateMessagePackStructCpp @M.TrodesTimestampData
    generateMessagePackStructCpp @M.VideoPacket

    putStrLn "Generated..."
